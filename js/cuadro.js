let personaje = {};
const nombreUsuarioLogeado = document.getElementById("nombreLogeado").innerText = `Hola ${localStorage.getItem('user')} !`;
const id = document.getElementById('id');
const name = document.getElementById('name');
const status = document.getElementById('status');
const species = document.getElementById('species');
const type = document.getElementById('type');
const gender = document.getElementById('gender');

const actualizarReloj = () => {
    let momentoActual = new Date();
    let hora = momentoActual.getHours();
    let minuto = momentoActual.getMinutes();
    let segundo = momentoActual.getSeconds();
    let fecha = momentoActual.toLocaleDateString();
    let horaImprimible = ` ${fecha} - ${hora} : ${minuto} : ${segundo}`;
    document.getElementById("reloj").innerHTML = horaImprimible;
    setTimeout('actualizarReloj()', 1000);
}

const hayUsuarioIngresado = () => {
    if (!localStorage.getItem('user')) {
        window.location = 'index.html';
    }
}

const deslogear = () => {
    localStorage.clear();
    window.location = 'index.html';
}

const mostrarImagen = () => {
    if (personaje.image) {
        document.getElementById("imagenPersonaje").innerHTML = `<img src="${personaje.image}" />`;
    }
}

const limpiarElementos = () => {
    document.getElementById("errorBusqueda").innerText = '';
    document.getElementById("imagenPersonaje").innerHTML = '';
}

const errorEnBusqueda = (id) => {
    const spanError = document.getElementById("errorBusqueda");
    spanError.style.color = 'red';
    const errorMayor = 'No se puede realizar la busqueda, hay un maximo de 671!';
    const errorMenor = 'No se puede realizar la busqueda, el minimo es 1!';
    id > 671 ? spanError.innerText = errorMayor : spanError.innerText = errorMenor;
}

const cargarCuadro = (idPersonaje) => {
    fetch(`https://rickandmortyapi.com/api/character/${idPersonaje}`)
        .then(resp => resp.json())
        .then(data => {
            personaje = data;
            id.innerText = personaje.id;
            name.innerText = personaje.name;
            status.innerText = personaje.status;
            species.innerText = personaje.species;
            type.innerText = personaje.type;
            gender.innerText = personaje.gender;
        });
}

window.onload = () => {
    actualizarReloj();
    hayUsuarioIngresado();
    const form = document.getElementById('formularioCuadro');
    form.onsubmit = (e) => {
        e.preventDefault();
        const idPersonaje = document.getElementById('personaje').value;
        const esBusquedaValida = idPersonaje > 671 || idPersonaje <= 0;
        if (esBusquedaValida) {
            errorEnBusqueda(idPersonaje);
        } else {
            limpiarElementos();
            cargarCuadro(idPersonaje);
        }
    }
}