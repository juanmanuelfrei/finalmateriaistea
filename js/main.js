let intentos = 3;
let usersDB = [];

const cargarUsersValidos = () => {
    fetch(`https://jsonplaceholder.typicode.com/users`)
        .then(resp => resp.json())
        .then(data => usersDB = data);
}

const mostrarError = () => {
    intentos--;
    const spanError = document.getElementById('error');
    spanError.innerText = `Clave o usuario incorrectos, te quedan ${intentos} intentos!`;
}

function validacionClave(id) {
    var expresionRegular = /^\d{1,2}$/;
    if (!expresionRegular.test(id)) {
        //retorno falso si la clave es invalida
        return false;
    }
    //retorno verdadero si la clave es valida
    return true;
}

window.onload = () => {
    cargarUsersValidos();
    const form = document.getElementById('formulario');
    form.onsubmit = (e) => {
        e.preventDefault();
        const usuarioIngresado = document.getElementById('user').value;
        const claveIngresada = document.getElementById('clave').value;
        const usuarioValido = usersDB.find(x => x.email === usuarioIngresado);
        const claveValida = validacionClave(claveIngresada);

        if (usuarioValido && claveValida) {
            if (claveIngresada == usuarioValido.id) {
                localStorage.setItem('user', usuarioValido.name);
                window.location = 'cuadro.html';
            } else {
                mostrarError();
            }
        } else {
            mostrarError();
        }

        if (intentos === 0) {
            form.innerHTML = 'Cuenta bloqueada';
            form.style.color = 'red';
            document.getElementById("failLogin").innerHTML = '<img src="./assets/imagenes/failAcount.jpg" />';
        }

    }
}