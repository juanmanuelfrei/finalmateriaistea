#Proyecto sencillo para final de materia

Consta de un login y un main page

- Si el usuario no se encuentra logeado no podra acceder al main page.
- Al fallar la clave 3 veces se bloquea el login.
- Los usuarios validos son los de la siguiente api: https://jsonplaceholder.typicode.com/users, siendo el id la clave de c/u.
- La api del main page es la siguiente: https://rickandmortyapi.com/api/character.
- En el main page se busca por id de personaje y completa el cuadro con la informacion del mismo, teniendo un maximo de 678 ids validos.
- Al hacerle click al nombre del personaje se muestra una imagen.
